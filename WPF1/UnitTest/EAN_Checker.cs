﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF1
{
    public class EAN_Checker
    {
        public string? strEAN { get; private set; }
        public EAN_Checker(string? strEAN)
        {

            bool boolContainsOnlyNumbers = false;
            int intEAN = 0; //по сути заглушка нужная только для проверки в строке ниже
            if (strEAN.Length == 13)
            {
                boolContainsOnlyNumbers = true;
            }

            int intBuffer = 0; //просто заглушка для проверки.
            foreach (char ch in strEAN)
            {                
                if ( !(int.TryParse(ch.ToString(), out intBuffer)) ) {
                    boolContainsOnlyNumbers = false;
                }
            }

            if (boolContainsOnlyNumbers)
            {
                this.strEAN = strEAN;
            }
            else
            {
                //будет использовано для указания на ошибки при вводе
                this.strEAN = null; 
            }            
        }

        public bool CheckEAN()
        {
            if (this.strEAN == null)
            {
                //если у нас там null - то пользователь ввел данные некорректно
                return false;
            }

            int intEven = 0;
            int intOdd = 0;

            // сложили четные и нечетные числа в 2 разные группы
            for (int i = 1; i < (strEAN.Length); i++)
            {
                if (i % 2 == 0)
                {
                    intEven += int.Parse(strEAN[i-1].ToString());
                } 
                else
                {
                    intOdd += int.Parse(strEAN[i-1].ToString());
                }
            }

            intEven = intEven * 3;
            int intPreResult = intEven + intOdd;
            int intincreasedMultipleOf10 = intPreResult;

            // увеличиваем до ближайшего целого числа кратного 10
            while (intincreasedMultipleOf10 % 10 != 0)
            {
                intincreasedMultipleOf10++;
            }

            int intCheckSumNumber = intincreasedMultipleOf10 - intPreResult;
            int intLastNumberInEAN = int.Parse(strEAN[12].ToString());

            if (intCheckSumNumber == intLastNumberInEAN)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
