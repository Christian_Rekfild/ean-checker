using NUnit.Framework;
using WPF1;

namespace UnitTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void test48701120030FF24()
        {
            WPF1.EAN_Checker EAN_ch = new EAN_Checker("48701120030FF24");

            Assert.AreEqual(EAN_ch.strEAN, null);

            //Assert.Pass();
        }
        public void test4870112003024()
        {
            WPF1.EAN_Checker EAN_ch = new EAN_Checker("4870112003024");
            bool boolEANisRight = EAN_ch.CheckEAN();
            Assert.Equals(true, boolEANisRight);

            //Assert.Pass();
        }

        public void test4870112003022()
        {
            // ����������� ������
            WPF1.EAN_Checker EAN_ch = new EAN_Checker("4870112003022");
            bool boolEANisRight = EAN_ch.CheckEAN();
            Assert.AreNotEqual(true, boolEANisRight);
            //Assert.Pass();

        }
        public void test4603988017983()
        {
            WPF1.EAN_Checker EAN_ch = new EAN_Checker("4603988017983");
            bool boolEANisRight = EAN_ch.CheckEAN();
            Assert.Equals(true, boolEANisRight);

            //Assert.Pass();
        }
        public void test123456()
        {
            WPF1.EAN_Checker EAN_ch = new EAN_Checker("123456");
            Assert.AreEqual(EAN_ch.strEAN, null);

            //Assert.Pass();
        }
    }
}